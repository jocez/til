/*  TIL - Tray Icon Library
    Copyright (C) 2021 Jocez j@jocez.com
    You can use this code for anything you want, including commercial use. No warranty quaranteed.
*/

#include <glib.h>
#include <gdk/gdk.h>
#if GDK_MAJOR_VERSION == 4
#include <gdk/x11/gdkx.h>
#include <gtk/gtk.h>
#include <X11/Xlib-xcb.h>

#include <til.h>

// X11 Display
Display* gtk4_widget_get_x11_display(GtkWidget* widget){
    GdkDisplay *gdk_display = gtk_widget_get_display(widget);
    GdkX11Screen *gdk_x11_screen = gdk_x11_display_get_screen(gdk_display);
    Display *x11_display= gdk_x11_display_get_xdisplay(gdk_display);
    return x11_display;
}

// Window is just ID.
Window gtk4_widget_get_x11_window(GtkWidget* widget){
    GtkNative *gtk_native = gtk_widget_get_native(widget);
    GdkSurface *gdk_surface = gtk_native_get_surface(gtk_native);
    Window x11_window = gdk_x11_surface_get_xid(gdk_surface);
    return x11_window;
}

xcb_connection_t* gtk4_widget_get_xcb_connection(GtkWidget* widget){
    XGetXCBConnection(gtk4_widget_get_x11_display(widget));
}

static GtkWidget *create_status_icon(){
    // GTK4 code
    GtkWidget *status_icon = gtk_window_new(); // Create new window.
    gtk_widget_show(status_icon); // Window must be shown or we get error later.
    gtk_window_set_default_size(GTK_WINDOW(status_icon), 22, 22);

    // Get X11 Display
    xcb_connection_t *connection = gtk4_widget_get_xcb_connection(status_icon);
    Window x11_window = gtk4_widget_get_x11_window(status_icon);

    TIL *til = til_create();
    xcb_reparent_window(connection, x11_window, til_get_id(til), 0, 0);

    return status_icon;
}

static void loop(GtkApplication *app, gpointer user_data){
    GtkWidget *status_icon = create_status_icon();
    gtk_application_add_window(app, GTK_WINDOW(status_icon));
}

void main(int argc, char *argv[]){
    gdk_set_allowed_backends("x11");
	GtkApplication *app = gtk_application_new("gtk4.status.icon", 0);
    g_signal_connect(app, "activate", G_CALLBACK(loop), NULL);
    g_application_run(G_APPLICATION(app), 0, NULL);
}

#endif