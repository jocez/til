# make make install make clean make example

CC = gcc
PKGCONFIG = $(shell which pkg-config)
PKGS = gtk4 x11-xcb xcb xcb-icccm 
CFLAGS = $(shell $(PKGCONFIG) --cflags $(PKGS)) -lxcb -lX11 -lXi -ldl -lxcb-icccm
LIBS  = `pkg-config --libs $(PKGS)`
FILES = src/*.c
O_FILES = *.o
EXAMPLE_FILES = examples/*.c

all:
	for f in $(FILES); do $(CC) $(CFLAGS) -c $$f; done
	ar rcs build/til.a $(O_FILES) #static library
	gcc -shared -o build/til.so $(O_FILES) #shared library

install:
	sudo cp build/til.so /usr/local/lib/
	sudo cp src/til.h /usr/local/include/til.h

remove:
	sudo rm /usr/local/lib/til.so
	sudo rm /usr/local/include/til.h

example:
	$(CC) examples/gtk4_status_icon.c /usr/local/lib/til.so $(CFLAGS) $(LIBS) -o examples/gtk4_status_icon 

clean:
	rm *.o