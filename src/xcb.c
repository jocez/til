#if defined(__linux__)

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#include <xcb/xcb.h>

#include <xcb/xcb_event.h>
#include <xcb/xcb_icccm.h>
#include <xcb/xcb_atom.h>
#include <xcb/xcb_aux.h>

/* --------- XEMBED and systray stuff */
#define SYSTEM_TRAY_REQUEST_DOCK   0
#define SYSTEM_TRAY_BEGIN_MESSAGE   1
#define SYSTEM_TRAY_CANCEL_MESSAGE  2

//static int usleep(useconds_t useconds); //pass in microseconds

static xcb_atom_t get_atom_from_name(xcb_connection_t *xc, char* name){
	xcb_intern_atom_cookie_t cookie = xcb_intern_atom(xc,0,strlen(name),name);
	xcb_intern_atom_reply_t *reply;
	xcb_generic_error_t *error;
	reply = xcb_intern_atom_reply(xc,cookie, &error);
	if(error != NULL){ printf("%d\n", error->response_type); }
	free(error);
	return reply->atom;
}

static xcb_window_t get_manager_selection_owner(xcb_connection_t *xc, int screen){
#define	TRAY_SEL_ATOM "_NET_SYSTEM_TRAY_S"
	char		*tray_sel_atom_name;
	if((tray_sel_atom_name = (char *)malloc(strlen(TRAY_SEL_ATOM) + 2)) == NULL) {
		exit(EXIT_FAILURE);
	}
	snprintf(tray_sel_atom_name, strlen(TRAY_SEL_ATOM) + 2, "%s%u", TRAY_SEL_ATOM, screen);
	xcb_atom_t atom = get_atom_from_name(xc,tray_sel_atom_name);
	xcb_get_selection_owner_cookie_t cookie1 = xcb_get_selection_owner(xc,  get_atom_from_name(xc,tray_sel_atom_name));
	free(tray_sel_atom_name);
	
	xcb_get_selection_owner_reply_t *reply1;
	reply1 = xcb_get_selection_owner_reply(xc,cookie1,NULL);
	xcb_window_t owner = reply1->owner;
	free(reply1);
	return owner;
}

static void xcb_systray_message_send(xcb_connection_t *connection, long message, long window, long data2, long data3){
    xcb_client_message_event_t ev;
    xcb_window_t tray = get_manager_selection_owner(connection, 0); // atom

    memset(&ev, 0, sizeof(ev));
    ev.response_type = XCB_CLIENT_MESSAGE;
    ev.window = tray;
    ev.type = get_atom_from_name(connection,"_NET_SYSTEM_TRAY_OPCODE"); 
    //printf("Viestin tyyppi on %d\n", ev.type);
    ev.format = 32;
    ev.data.data32[0] = XCB_CURRENT_TIME;
    ev.data.data32[1] = message;
    ev.data.data32[2] = window; // <--- your window is only here
    ev.data.data32[3] = data2;
    ev.data.data32[4] = data3;

    xcb_send_event(connection, 0, tray, XCB_EVENT_MASK_NO_EVENT,(const char *)&ev);

    // For older KDE's ...
    long atom_data = 1;
    xcb_atom_t tray_atom = get_atom_from_name(connection, "KWM_DOCKWINDOW");
    xcb_change_property(connection, XCB_PROP_MODE_REPLACE, window, tray_atom, tray_atom, 32, 1, (unsigned char*) &atom_data);

    // For more recent KDE's...
    tray_atom = get_atom_from_name(connection, "_KDE_NET_WM_SYSTEM_TRAY_WINDOW_FOR");
    long xa_window_atom  = get_atom_from_name(connection, "XA_WINDOW");
    xcb_change_property(connection, XCB_PROP_MODE_REPLACE, window, tray_atom, xa_window_atom, 32, 1, (unsigned char*) &window);

    // A minimum size must be specified for GNOME and Xfce, otherwise the icon is displayed with a width of 1
	xcb_size_hints_t hints;
	xcb_icccm_size_hints_set_min_size(&hints,22,22);
	xcb_icccm_set_wm_normal_hints(connection,window,&hints);

    usleep(100000); // Wait for signal to get to bar.
}
    
static xcb_window_t xcb_status_icon_create(){
    xcb_connection_t *connection = xcb_connect(NULL, NULL);
    const xcb_setup_t *xcb_setup = xcb_get_setup (connection);
    xcb_screen_t *xcb_screen = xcb_setup_roots_iterator(xcb_setup).data;
    xcb_window_t xcb_window = xcb_generate_id (connection);
    xcb_create_window(connection, XCB_COPY_FROM_PARENT, xcb_window, xcb_screen->root, 0, 0, 22, 22, 0, XCB_WINDOW_CLASS_INPUT_OUTPUT, xcb_screen->root_visual, 0, NULL);
    xcb_systray_message_send(connection, 0, xcb_window, 0, 0);
    xcb_flush(connection);
    return xcb_window;
}

xcb_window_t til_xcb_status_icon_create(){
    return xcb_status_icon_create();
}

#endif