#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "til.h"

struct TIL{
    size_t id;
    char *icon;
    char *tooltip;
};

size_t til_size(){
    return sizeof(TIL);
}

#include "xcb.h"
static void til_xcb_create(TIL *til){
    til->id = til_xcb_status_icon_create();
}

size_t til_get_id(TIL *til){
    return til->id;
}

TIL *til_create(){
    TIL *til = malloc(til_size());
    // IF x11 / xcb
    til_xcb_create(til);


    return til;
}

void til_destroy(TIL *til){
    free(til);
}

void til_add_icon(TIL *til, char *icon){

}