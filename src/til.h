#ifndef TIL_H
#define TIL_H

typedef struct TIL TIL;
size_t til_get_id(TIL *til);
TIL *til_create();

#endif